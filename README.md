# movies4us

A Vue.js based Movie listing app. Uses Vue.js 3, Vue CLI 4.5, Vue Router, Bulma CSS framework, HTML, CSS, and JavaScript. Also uses JSON Server for backend API.

Live **Site Demo** ~ [Vue.js 3 Twitter Clone](http://financialreact.ryanhunter.ca/) 

![Vue.js 3 Movies4Us ~ 1](http://www.ryanhunter.ca/images/portfolio/moviequest_01.png)

![Vue.js 3 Movies4Us ~ 2](http://www.ryanhunter.ca/images/portfolio/moviequest_03.png)


## Steps to Install 
- Run the command below from the command line / terminal / command prompt.
- git clone https://funtasticcoder@bitbucket.org/funtasticcoder/movies4us.git 
- cd movies4us 
- ensure your have Node & NPM pre-installed. Run commands 'node --version && npm -v'.
- npm install.  (This ensures all dependencies are installed).
- command ~ 'npm run serve' ==> Compiles and hot-reloads for development
- Runs on port 8080 via .env variable --> http://localhost:8080/ 
- Please rename '.env4Display' to '.env'.
- The .env port is set to 3000.
- Change the web port as needed.
- To build files and minify for Production deploy, run command, "npm run build" 
- command ~ 'npm run build' ==> Compiles and minifies for production 
## Features
- Vue.js 3
- Vue Router 4
- Vuex 4 for state management
- Bulma CSS framework 
- FontAwesome 5 icons
- Google Fonts
- Filler text from http://officeipsum.com/ 
## License
This project is licensed under the terms of the **MIT** license.

## Screenshots 

![Vue.js 3 Movies4Us ~ Screen 01](http://www.ryanhunter.ca/images/portfolio/funflicks03.png) 

![Vue.js 3 Movies4Us ~ Screen 02](http://www.ryanhunter.ca/images/portfolio/funflicks04.png) 

![Vue.js 3 Movies4Us ~ Screen 03](http://www.ryanhunter.ca/images/portfolio/funflicks01.png) 

![Vue.js 3 Movies4Us ~ Screen 04](http://www.ryanhunter.ca/images/portfolio/funflicks02.png) 
